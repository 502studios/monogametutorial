# README #

Flappy Bird with MonoGame

### What you need to run this sample? ###

* Visual Studio
* Monogame 3.2

### How to play? ###

* Press space bar to make your plane fly higher.


### Art deparment ###

* Assets by Kenney 


### Code License ###

* If not stated otherwise, all code is issued under the MIT licence.