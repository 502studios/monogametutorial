﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using MonoGameTutorial.GameClasses.Screens;
#endregion

namespace MonoGameTutorial
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;


        public static Game1 instance;
        Screen currenScreen;

        public List<Texture2D> planeImageList = new List<Texture2D>();
        public Texture2D background, foregroundBottom, obstacleBottom, obstacleTop, title;
        public Microsoft.Xna.Framework.Graphics.SpriteFont font;

        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = 800;  // set this value to the desired width of your window
            graphics.PreferredBackBufferHeight = 480;   // set this value to the desired height of your window
            graphics.ApplyChanges();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
            instance = this;
            GoToScreen(Screen.Screentype.TitleScreen);
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            title = Content.Load<Texture2D>("title");
            // loads the background image
            background = Content.Load<Texture2D>("background");
            foregroundBottom = Content.Load<Texture2D>("groundGrass");
            obstacleBottom = Content.Load<Texture2D>("rockGrassDown");
            obstacleTop = Content.Load<Texture2D>("rockGrass");
            //loads the player image list
            planeImageList.Add(Content.Load<Texture2D>("planeRed1"));
            planeImageList.Add(Content.Load<Texture2D>("planeRed2"));
            planeImageList.Add(Content.Load<Texture2D>("planeRed3"));
            // TODO: use this.Content to load your game content here
            font = Content.Load<Microsoft.Xna.Framework.Graphics.SpriteFont>("SpriteFont1");

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            base.Update(gameTime);

            currenScreen.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            base.Draw(gameTime);

            currenScreen.Draw(gameTime, spriteBatch);
        }

        public void GoToScreen(Screen.Screentype type) {

            if (type.Equals(Screen.Screentype.TitleScreen))
                currenScreen = new TitleScreen();

            if (type.Equals(Screen.Screentype.InGameScreen))
                currenScreen = new InGameScreen();

        }

    }
}
