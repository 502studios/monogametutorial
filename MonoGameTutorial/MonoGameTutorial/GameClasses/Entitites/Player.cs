﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using MonoGameTutorial.GameClasses.Screens;
#endregion

namespace MonoGameTutorial.GameClasses.Entities
{
    public class Player
    {
        Vector2 position;
        Vector2 velocity;
        float gravity;
        float angle;
        List<Texture2D> Animation;
        float timePassed;
        int animationIndex;

        public Player(List<Texture2D> texture)
        {
            Animation = texture;
            gravity = 12.8f;
            position = new Vector2(Game1.instance.Window.ClientBounds.Width / 4, Game1.instance.Window.ClientBounds.Height / 2);
        }

        public void ApplyGravity(GameTime gameTime)
        {
            var delta = (float)gameTime.ElapsedGameTime.Milliseconds / 1000;

            velocity.Y += gravity*delta;
            if (velocity.Y < 0)
                angle = -0.5f;
            else {
                angle += 0.01f;
                if(angle > 0.5f)
                    angle = 0.5f;
            }
            position += velocity;
        }

        public void Animate(GameTime gameTime)
        {
            var delta = (float)gameTime.ElapsedGameTime.Milliseconds / 1000;

            timePassed += delta;

            if (timePassed > 8 * delta)
            {
                timePassed = 0;
                animationIndex++;
                if (animationIndex > Animation.Count - 1)
                    animationIndex = 0;
            }
        }

        public void Draw(SpriteBatch spBatch, GameTime gameTime)
        {
            Rectangle sourceRectangle = new Rectangle(0, 0, Animation[animationIndex].Width, Animation[animationIndex].Height);
            Vector2 origin = new Vector2(0, 0);
            spBatch.Begin();
            spBatch.Draw(Animation[animationIndex], position, sourceRectangle, Color.White, angle, origin, 1.0f, SpriteEffects.None, 1);
            spBatch.End();
        }

        public void Jump() {
            velocity.Y = -5f;
        }

        public Rectangle getBoundingBox()
        {
            return new Rectangle((int)position.X + 10, (int)position.Y + 10, (int)(Animation[animationIndex].Width - 10), (int)(Animation[animationIndex].Height - 10));
        }
    }
}