﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

namespace MonoGameTutorial.GameClasses
{
    public class Obstacle
    {
        Vector2 position;
        Texture2D sprite;
        float velocity;
        bool shouldAddScore;

        public Obstacle(Texture2D texture, Vector2 pos)
        {
            sprite = texture;
            position = pos;
            shouldAddScore = true;
        }


        public void Update(GameTime gameTime)
        {
            var delta = (float)gameTime.ElapsedGameTime.Milliseconds / 1000;

            velocity = -200f * delta;

            position.X += velocity;

        }

        public void Draw(SpriteBatch spBatch, GameTime gameTime)
        {
            spBatch.Begin();
            spBatch.Draw(sprite, position, Color.White);
            spBatch.End();
        }

        public Rectangle getBoundingBox() {
            Rectangle rect = new Rectangle((int)position.X, (int)position.Y, sprite.Width, sprite.Height);

            return new Rectangle((int)position.X , (int)position.Y, sprite.Width, sprite.Height);
        }

        public void DoNotAddScore()
        {
            shouldAddScore = false;
        }

        public bool AddsScore()
        {
            return shouldAddScore;
        }

    }
}