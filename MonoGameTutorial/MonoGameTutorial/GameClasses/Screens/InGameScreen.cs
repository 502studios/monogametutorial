﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using MonoGameTutorial.GameClasses.Entities;
#endregion

namespace MonoGameTutorial.GameClasses.Screens
{
    public class InGameScreen : Screen
    {
        ParallaxScroll _parallaxBackground;
        ParallaxScroll _parallaxForegrounBottom;
        Player _plane;
        List<Obstacle> _obstacleList = new List<Obstacle>();
        float _timeToSpawn;
        int score;
        Random r;
        string gameMessage;
        bool shouldUpdatePlayer;
        bool gameOver;
        public  InGameScreen()
        {
            Initialize();
        }

        public override void Initialize()
        {

            _parallaxBackground = new ParallaxScroll(Game1.instance.background, 0);
            _parallaxForegrounBottom = new ParallaxScroll(Game1.instance.foregroundBottom, 420, 2f);
            _plane = new Player(Game1.instance.planeImageList);
            shouldUpdatePlayer = false;
            gameOver = false;
            r = new Random();
            gameMessage = "Get ready!";
        }

        public override void Update(GameTime gameTime)
        {
            if (gameOver)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Enter))
                    Game1.instance.GoToScreen(Screentype.TitleScreen);

                _plane.ApplyGravity(gameTime);
                return;
            }

            var delta = (float)gameTime.ElapsedGameTime.Milliseconds / 1000;

            _parallaxBackground.Update(gameTime);
            _parallaxForegrounBottom.Update(gameTime);
            _plane.Animate(gameTime);

            //Does the obstacle creation
            _timeToSpawn += delta;
            if (_timeToSpawn > 2)
            {
                GenerateObstacles();
                _timeToSpawn = 0;

                if (!shouldUpdatePlayer)
                    shouldUpdatePlayer = true;
            }

            if (shouldUpdatePlayer )
            {
                gameMessage = "Score : " + score;
                if(!gameOver)
                    _plane.ApplyGravity(gameTime);
                if (Keyboard.GetState().IsKeyDown(Keys.Space))
                    _plane.Jump();
            }

            GroundCheck();
            UpdateObstacles(gameTime);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            _parallaxBackground.Draw(spriteBatch, gameTime);

            for (int x = 0; x < _obstacleList.Count; x++)
            {
                _obstacleList[x].Draw(spriteBatch, gameTime);
            }
            _plane.Draw(spriteBatch, gameTime);
            _parallaxForegrounBottom.Draw(spriteBatch, gameTime);
            spriteBatch.Begin();
            spriteBatch.DrawString(Game1.instance.font, gameMessage, new Vector2(360, 0), Color.Black);
            spriteBatch.End();
        }


        // generate new obstacle pairs

        public void GenerateObstacles() 
        {
            int nextValue = r.Next(-2, 2);
            _obstacleList.Add(new Obstacle(Game1.instance.obstacleBottom, new Vector2(800, -80+(80*nextValue))));
            _obstacleList.Add(new Obstacle(Game1.instance.obstacleTop, new Vector2(800, 400 + (80 * nextValue))));
        }

        public void UpdateObstacles(GameTime gameTime)
        {
            for (int x = _obstacleList.Count -1; x >= 0 ;x--)
            {
                if (_obstacleList[x].getBoundingBox().Intersects(_plane.getBoundingBox()))
                {
                    gameMessage = "game over, press Enter to continue";
                    gameOver = true;
                }

                _obstacleList[x].Update(gameTime);

                if (_obstacleList[x].getBoundingBox().X < _plane.getBoundingBox().X)
                {
                    if (_obstacleList[x].AddsScore())
                    {
                        score++;
                        _obstacleList[x].DoNotAddScore();
                    }
                }

                if (_obstacleList[x].getBoundingBox().Right< 0)
                {
                    _obstacleList.Remove(_obstacleList[x]);
                    break;
                }
            }

        }
        public void GroundCheck()
        {
            if (_plane.getBoundingBox().Bottom > 480)
            {
                gameMessage = "game over, press Enter to continue";
                gameOver = true;
            }
        }

    }
}
