﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion
namespace MonoGameTutorial.GameClasses.Screens
{
    public class TitleScreen : Screen
    {

        public override void Initialize()
        {

        }

        public override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
                Game1.instance.GoToScreen(Screentype.InGameScreen);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();
            spriteBatch.Draw(Game1.instance.background, new Vector2(0, 0), Color.White);
            spriteBatch.Draw(Game1.instance.title, new Vector2(240, 120), Color.White);
            if(gameTime.TotalGameTime.Seconds%2 ==0)
                spriteBatch.DrawString(Game1.instance.font, "press space bar to start", new Vector2(280, 300), Color.Black);
            spriteBatch.End();
        }
    }
}
