﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

namespace MonoGameTutorial.GameClasses
{
    public class ParallaxScroll
    {

        Vector2 bgPosition1, bgPosition2 = new Vector2(0, 0);
        Texture2D bg1, bg2;
        float scrollingSpeed;

        public ParallaxScroll(Texture2D textureName, float height, float scrollSpeed = 1f) {
            bg1 = bg2 = textureName;
            bgPosition1 = new Vector2(0, height);
            bgPosition2 = new Vector2(bg2.Width, height);
            scrollingSpeed = scrollSpeed;
        }

        public void Update(GameTime gameTime)
        {
            bgPosition1.X -= scrollingSpeed ;
            bgPosition2.X -= scrollingSpeed ;

            if(bgPosition1.X + bg1.Width <= 0){
                bgPosition1.X = bg1.Width -1;
            }

            if (bgPosition2.X + bg2.Width <= 0)
            {
                bgPosition2.X = bg2.Width -1;
            }
        }

        public void Draw(SpriteBatch spBatch, GameTime gameTime)
        {
            spBatch.Begin();
            spBatch.Draw(bg1, bgPosition1, Color.White);
            spBatch.Draw(bg2, bgPosition2, Color.White);
            spBatch.End();
        }

    }
}
